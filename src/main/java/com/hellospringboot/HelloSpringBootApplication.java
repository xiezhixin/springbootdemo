package com.hellospringboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication //标注这个类是springboot的应用 启动类下的所有资源都被导入
public class HelloSpringBootApplication {

    public static void main(String[] args) {
        //将springboot应用启动
        SpringApplication.run(HelloSpringBootApplication.class, args);
    }
    //    cd D:\WorkSpaces\HelloSpringBoot\target
    //    java -jar .\HelloSpringBoot-0.0.1-SNAPSHOT.jar
}
